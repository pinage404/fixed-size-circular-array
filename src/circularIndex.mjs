function circularIndex(index, length) {
	index >>= 0
	length >>= 0
	if (length > 0) {
		while (index < 0) {
			index += length
		}
		while (index / length >= 2.0) {
			index -= length
		}
		index %= length
	}
	return index
}


export default circularIndex
export {circularIndex}
