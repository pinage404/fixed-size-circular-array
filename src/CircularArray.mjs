import circularIndex from './circularIndex.mjs'


class CircularArray extends Array {
	static get [Symbol.species]() {
		return Array
	}


	static fromArray(array=[]) {
		if (array.length === 1) {
			const circularArray = new CircularArray(1)
			circularArray.firstItem = array[0]
			return circularArray
		}

		return new CircularArray(...array)
	}


	/**
	 * example with first virtual index settled to 2
	 *
	 * 0 1 2 3 4 5 6 <- real (native array) index
	 *     0 1 2 3 4 <- virtual index
	 * 5 6 ^         <- virtual index
	 *   ^ |
	 *   | firstIndex
	 *   |
	 * lastIndex
	 */
	indexToVirtualIndex(index=0) {
		return circularIndex((index >> 0) + this.__virtualIndex, this.length)
	}


	getItem(index) {
		return this[this.indexToVirtualIndex(index)]
	}
	setItem(index, value) {
		this[this.indexToVirtualIndex(index)] = value
	}


	get firstIndex() {
		// modulo is needed if the length is changed after setting firstItem
		return this.indexToVirtualIndex(0)
	}
	set firstIndex(value) {
		this.__virtualIndex = circularIndex(value, this.length)
	}

	get firstItem() {
		return this[this.firstIndex]
	}
	set firstItem(value) {
		this[this.firstIndex] = value
	}


	get lastIndex() {
		return this.indexToVirtualIndex(-1)
	}
	set lastIndex(value) {
		this.firstIndex = value + 1
	}

	get lastItem() {
		return this[this.lastIndex]
	}
	set lastItem(value) {
		this[this.lastIndex] = value
	}


	selectPrevious() {
		this.firstIndex -= 1
	}

	selectNext() {
		this.firstIndex += 1
	}


	unshift(...itemList) {
		for (const item of itemList) {
			this.selectPrevious()
			this.firstItem = item
		}
		return this.length
	}

	push(...itemList) {
		for (const item of itemList) {
			this.selectNext()
			this.lastItem = item
		}
		return this.length
	}

	shift() {
		const firstItem = this.firstItem
		this.firstItem = undefined
		return firstItem
	}

	pop() {
		const lastItem = this.lastItem
		this.lastItem = undefined
		return lastItem
	}
}

Object.defineProperty(CircularArray.prototype, '__virtualIndex', {
	value: 0,
	writable: true,
})


export default CircularArray
export {CircularArray}
