import chai from 'chai'
const should = chai.should()

import CircularArray from '../src/CircularArray.mjs'


describe('CircularArray', () => {
	it('should be a class inherited from Array', () => {
		const circularArray =  new CircularArray(5)
		circularArray.should.be.an.instanceOf(Array)
		circularArray.should.be.an.instanceOf(CircularArray)
	})


	it('should accept length as constructor argument', () => {
		const circularArray = new CircularArray(5)
		circularArray.should.have.lengthOf(5)
	})


	it('should accept multiple arguments as initial data', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
		Array.from(circularArray.entries()).should.be.deep.equal([
			[0, 0],
			[1, 2],
			[2, 4],
			[3, 8],
			[4, 16]
		])
	})


	it('should be circular', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
		circularArray.firstItem.should.be.equal(0)
		circularArray.selectNext()
		circularArray.firstItem.should.be.equal(2)
		circularArray.selectNext()
		circularArray.firstItem.should.be.equal(4)
		circularArray.selectNext()
		circularArray.firstItem.should.be.equal(8)
		circularArray.selectNext()
		circularArray.firstItem.should.be.equal(16)
		circularArray.selectNext()
		circularArray.firstItem.should.be.equal(0)
	})


	it('should be circular in both ways', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
		circularArray.firstItem.should.be.equal(0)
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(16)
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(8)
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(4)
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(2)
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(0)
	})


	describe('should convert the index to the virtual index', () => {
		it('should return the virtual index given an index', () => {
			const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
			const truc = [
				[ 0, [0, 1, 2, 3, 4] ],
				[ 1, [1, 2, 3, 4, 0] ],
				[ 2, [2, 3, 4, 0, 1] ],
				[ 3, [3, 4, 0, 1, 2] ],
				[ 4, [4, 0, 1, 2, 3] ],
			]
			for (const [currentVirtualIndex, indexList] of truc) {
				for (const [index, virtual] of indexList.entries()) {
					circularArray.indexToVirtualIndex(index).should.be.equal(virtual)
				}
				circularArray.selectNext()
			}
			for (const [currentVirtualIndex, indexList] of truc.reverse()) {
				circularArray.selectPrevious()
				for (const [index, virtual] of indexList.entries()) {
					circularArray.indexToVirtualIndex(index).should.be.equal(virtual)
				}
			}
		})


		it('with odd number length', () => {
			const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
			circularArray.indexToVirtualIndex(2).should.be.equal(2)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(2).should.be.equal(3)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(2).should.be.equal(4)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(2).should.be.equal(0)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(2).should.be.equal(1)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(2).should.be.equal(2)
		})


		it('with even number length', () => {
			const circularArray = new CircularArray(...[0, 2, 4, 8, 16, 32])
			circularArray.indexToVirtualIndex(3).should.be.equal(3)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(4)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(5)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(0)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(1)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(2)
			circularArray.selectNext()
			circularArray.indexToVirtualIndex(3).should.be.equal(3)
		})
	})


	it('should give the good index with negative float index', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8])
		circularArray.selectNext()
		circularArray.selectNext()
		circularArray.selectNext()
		circularArray.indexToVirtualIndex(-1.5).should.be.equal(2)
	})


	it('should unshift in a circular way', () => {
		const circularArray = new CircularArray(7)
		circularArray.unshift(60, 50, 40, 30, 20, 10, 0)
		circularArray.firstItem.should.be.equal(0)
		circularArray.getItem(2).should.be.equal(20)
		circularArray.getItem(3).should.be.equal(30)
		circularArray.getItem(4).should.be.equal(40)
		circularArray.lastItem.should.be.equal(60)
		circularArray.unshift('last')
		circularArray.unshift('don t care')
		circularArray.unshift('next')
		circularArray.unshift('middle')
		circularArray.unshift('previous')
		circularArray.unshift('don t care')
		circularArray.unshift('first')
		circularArray.firstItem.should.be.equal('first')
		circularArray.getItem(2).should.be.equal('previous')
		circularArray.getItem(3).should.be.equal('middle')
		circularArray.getItem(4).should.be.equal('next')
		circularArray.lastItem.should.be.equal('last')
	})


	it('should push in a circular way', () => {
		const circularArray = new CircularArray(7)
		circularArray.push(0, 10, 20, 30, 40, 50, 60)
		circularArray.firstItem.should.be.equal(0)
		circularArray.getItem(2).should.be.equal(20)
		circularArray.getItem(3).should.be.equal(30)
		circularArray.getItem(4).should.be.equal(40)
		circularArray.lastItem.should.be.equal(60)
		circularArray.push('first')
		circularArray.push('don t care')
		circularArray.push('previous')
		circularArray.push('middle')
		circularArray.push('next')
		circularArray.push('don t care')
		circularArray.push('last')
		circularArray.firstItem.should.be.equal('first')
		circularArray.getItem(2).should.be.equal('previous')
		circularArray.getItem(3).should.be.equal('middle')
		circularArray.getItem(4).should.be.equal('next')
		circularArray.lastItem.should.be.equal('last')
	})


	it('shift should replace the last item by undefined and return it', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
		circularArray.selectPrevious()
		circularArray.selectPrevious()
		circularArray.firstItem.should.be.equal(8)

		const item = circularArray.shift()

		item.should.be.equal(8)
		should.not.exist(circularArray.firstItem)
	})


	it('pop should replace the last item by undefined and return it', () => {
		const circularArray = new CircularArray(...[0, 2, 4, 8, 16])
		circularArray.selectPrevious()
		circularArray.selectPrevious()
		circularArray.lastItem.should.be.equal(4)

		const item = circularArray.pop()

		item.should.be.equal(4)
		should.not.exist(circularArray.lastItem)
	})


	describe('fromArray', () => {
		it('should instanciates an empty CircularArray with empty array', () => {
			const circularArray = CircularArray.fromArray()
			circularArray.should.be.an.instanceOf(CircularArray)
			circularArray.should.have.a.lengthOf(0)
		})


		it('should instanciates an CircularArray with 1 items', () => {
			const array = [1]
			const circularArray = CircularArray.fromArray(array)
			circularArray.should.be.an.instanceOf(CircularArray)
			circularArray.should.have.a.lengthOf(1)
			circularArray.firstItem.should.equal(1)
			circularArray.lastItem.should.equal(1)
		})


		it('should instanciates an empty CircularArray with empty array', () => {
			const array = [1, 2]
			const circularArray = CircularArray.fromArray(array)
			circularArray.should.be.an.instanceOf(CircularArray)
			circularArray.should.have.a.lengthOf(2)
			circularArray.firstItem.should.equal(1)
			circularArray.lastItem.should.equal(2)
		})
	})
})
