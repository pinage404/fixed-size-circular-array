import mocha from 'mocha'
import chai from 'chai'
chai.should()

import circularIndex from '../src/circularIndex.mjs'


describe('circularIndex', () => {
	it('should retun the input index when it is between 0 and length', () => {
		circularIndex(5, 10).should.be.equal(5)
	})


	it('should loop if index is too low', () => {
		circularIndex(-33, 10).should.be.equal(7)
	})


	it('should loop if index is too high', () => {
		circularIndex(33, 10).should.be.equal(3)
	})


	it('should cast to int if the given index is a float', () => {
		circularIndex(33.3, 10).should.be.equal(3)
	})
})

