# Circular Array

Fixed size circular array datastructure

[![Pipeline status][pipeline-badge]][pipeline-url]
[![Published on NPM][npm-badge]][npm-url]


- [How to use](#how-to-use)
    - [circularIndex](#circularindex)
    - [CircularArray](#circulararray)
        - [Availlable properties](#availlable-properties)
        - [Availlable methods](#availlable-methods)
- [How to install](#how-to-install)
- [License](#license)


This data structure give an fixed size array, who reuse slots by overwriting existing items and allow to move first and last indexes


## How to use

### circularIndex

Function who returns the bounded index

./src/circularIndex.mjs

./browser/circularIndex.js

```javascript
const boundedindex = circularIndex.circularIndex(index, length)
```


### CircularArray

./src/CircularArray.mjs

./browser/CircularArray.js

```javascript
const array = ['zero', 'one', 'two', 'three', 'four']
const circularArray = CircularArray.CircularArray.fromArray(array)

circularArray.firstIndex = 2

circularArray.getItem(3) // 'zero'

circularArray.selectPrevious() // first index is now set to 1 and last index to 0

circularArray.getItem(3) // 'four'
```

#### Availlable properties

- `firstIndex`
- `firstItem`
- `lastIndex`
- `lastItem`

#### Availlable methods

- `static fromArray(array)`
- `indexToVirtualIndex(index)`
- `getItem(index)`
- `setItem(index, value)`
- `selectPrevious()`
- `selectNext()`

Overwritten methods:

- `unshift(...itemList)`
- `push(...itemList)`
- `shift()`
- `pop()`

And all methods of `Array.prototype` with untested result


## How to install

With [`Yarn`](https://yarnpkg.com)

```bash
yarn add fixed-size-circular-array
```

or with `NPM`

```bash
npm install --save fixed-size-circular-array
```


## License

[MIT][license]



[pipeline-badge]: https://gitlab.com/pinage404/fixed-size-circular-array/badges/master/pipeline.svg
[pipeline-url]:   https://gitlab.com/pinage404/fixed-size-circular-array/commits/master

[npm-badge]: https://img.shields.io/npm/v/fixed-size-circular-array.svg
[npm-url]:   https://www.npmjs.com/package/fixed-size-circular-array

[license]: ./LICENSE
